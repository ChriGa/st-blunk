<?php

/**

 * @package     Joomla.Site

 * @subpackage  mod_menu

 * @copyright   Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.

 * @license     GNU General Public License version 2 or later; see LICENSE.txt

 */



defined('_JEXEC') or die;



// Note. It is important to remove spaces between elements.



$is_navbar = strpos(' ' . $class_sfx . ' ', ' navbar-nav ') !== false;

?>

<div class="overlay" id="overlay">
	<nav class="overlay-menu">
		<ul class="nav overlay <?php echo $class_sfx;?> id="overlay" "<?php

			$tag = '';

			if ($params->get('tag_id') != null)

			{

				$tag = $params->get('tag_id').'';

				echo ' id="'.$tag.'"';

			}

		?>>

		<?php

		if (is_array($list)) :

			foreach ($list as $i => &$item) :

				$class = 'item-'.$item->id;

				if ($item->id == $active_id) {

					$class .= ' current';

				}



				if (in_array($item->id, $path)) {

					$class .= ' active';

				}

				elseif ($item->type == 'alias') {

					$aliasToId = $item->params->get('aliasoptions');

					if (count($path) > 0 && $aliasToId == $path[count($path)-1]) {

						$class .= ' active';

					}

					elseif (in_array($aliasToId, $path)) {

						$class .= ' alias-parent-active';

					}

				}



				if ($item->type == 'separator') {

					$class .= ' divider';

				}



				if ($item->deeper) {

					if ($item->level > 1){

						$class .= ' dropdown-submenu';

					} else {

						$class .= ' deeper dropdown';

					}

				}



				if ($item->parent) {

					$class .= ' parent';

				}



				if (!empty($class)) {

					$class = ' class="'.trim($class) .' fx-1 "';

				}



				echo '<li'.$class.'><div class="zoom-fx"><span class="sub_title"><h3>'.$item->params->get('page_heading') .'</h3></span><h2>'; //Override CG: alternativer Titel hier als Sub-Überschrift



				// Render the menu item.

				switch ($item->type) :

					case 'separator':

					case 'url':

					case 'component':

					case 'heading':

						require JModuleHelper::getLayoutPath('mod_menu', 'default_'.$item->type);

						break;



					default:

						require JModuleHelper::getLayoutPath('mod_menu', 'default_url');

						break;

				endswitch;



				// The next item is deeper.

				if ($item->deeper) {

					echo '<ul class="dropdown-menu">';

				}

				// The next item is shallower.

				elseif ($item->shallower) {

					echo '</li>';

					echo str_repeat('</ul></li>', $item->level_diff);

				}

				// The next item is on the same level.

				else {

					echo '</h2></div></li>';

				}

			endforeach;

		endif;

		?>
			<li class="visible-phone">
				<h2><a href="/impressum.html">Impressum</a></h2>
			</li>
			<li class="visible-phone">
				<h2><a href="/datenschutz.html">Datenschutz</a></h2>
			</li>			
			<li class="visible-phone">
				<h2><a href="/sitemap.html">Sitemap</a></h2>
			</li>			
		</ul>
	</nav>
</div>