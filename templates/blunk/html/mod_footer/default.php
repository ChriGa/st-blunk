<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<div class="copyright">
	<div class="footer1<?php echo $moduleclass_sfx ?>"><?php echo $lineone.' Designed by <a href="https://cleartemplates.com/" title="Free Joomla templates" target="_blank">ClearTemplates.com</a>'; ?></div>
</div>
