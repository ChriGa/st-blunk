<?php
/**
 * @author   	cg@089webdesign.de
 */
 

defined('_JEXEC') or die;
//include system
include_once(JPATH_ROOT . "/templates/" . $this->template . '/lib/system.php');
//include template Functions CG
include_once(JPATH_ROOT . "/templates/" . $this->template . '/template_functions.php');
//
?>
<!DOCTYPE html>
<html lang="De-de">
<head>
	<?php 
	// including head
	include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/head.php');
	?>
</head>

<body id="body" class="site <?php echo $option
	. ' view-' . $view
	. ($layout ? ' layout-' . $layout : ' no-layout')
	. ($task ? ' task-' . $task : ' no-task')
	. ($itemid ? ' itemid-' . $itemid : '');
?>">

	<!-- Body -->
		<div class="site_wrapper animsition">
			<div class="short-hr hidden_mobile"></div>
			<div class="long-hr hidden_mobile"></div>
			<div class="short-hr-2 hidden_mobile"></div>
				<div id="inner-wrapper">
					<?php			
					
					// including top
						/*
						 * breadcrumbs layout-bedingt in header.php integriert				
						 */
					//include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/breadcrumbs.php'); 

					// including header
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/header.php');			

					// including header
						/*
						 * Menu layout-bedingt in header.php integriert				
						 */
					//include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/menu.php');			
					
					// including slider
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/slider.php');
							
					// including top
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top.php');	
					
					// including top2
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/top2.php');	
											
					// including content
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/content.php');	
					
					// including bottom
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom.php');	
					
					// including bottom2
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/bottom2.php');
					
					// including content
					include_once(JPATH_ROOT . "/templates/" . $this->template . '/blocks/footer.php');				
					
					?>					
				</div>			
		</div>
	
	
	<jdoc:include type="modules" name="debug" style="none" />
	<script>
	    <? /*Varaiblen standard Init für FX*/ ?>
	    	var inEffect = "fade-in";
			var outEffect = "fade-out";
			var inDur = 800;
			var outDur = 500;

		<?php /*Mobile Detection JS!!*/ ?>
			    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
			    	inEffect = "fade-in-left-sm";
			    	outEffect = "fade-out";
			    	inDur = 900;
			    	outDur = 700;
			    }

		<? // siehe: http://git.blivesta.com/animsition/ ?>
		jQuery(document).ready(function() {
		  
		  jQuery(".animsition").animsition({
		  
		    inClass               :   inEffect,
		    outClass              :   outEffect,
		    inDuration            :    inDur,
		    outDuration           :    outDur,
		    linkElement           :   '.animsition-link',
		    // e.g. linkElement   :   'a:not([target="_blank"]):not([href^=#])'
		    loading               :    true,
		    loadingParentElement  :   'body', //animsition wrapper element
		    loadingClass          :   'animsition-loading',
		    unSupportCss          : [ 'animation-duration',
		                              '-webkit-animation-duration',
		                              '-o-animation-duration'
		                            ],
		    <? /*"unSupportCss" option allows you to disable the "animsition" in case the css property in the array is not supported by your browser.
		    //The default setting is to disable the "animsition" in a browser that does not support "animation-duration". */ ?>
		    
		    overlay               :   false,
		    
		    overlayClass          :   'animsition-overlay-slide',
		    overlayParentElement  :   'body'
		  });
		});
	</script>
	<script>
		jQuery(()=>{
			jQuery('#toggle').click(function() {
				jQuery(this).toggleClass('active');
				jQuery('#overlay').toggleClass('open');
				jQuery('.moduletable.lang-switch').toggleClass('setBack');
			});
		});
	</script>
</body>
</html>
