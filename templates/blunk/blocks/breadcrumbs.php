<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if ($this->countModules('breadcrumbs')) : ?>
<div class="clear-breadcrumbs">
	<div class="container ">
		<div class="row-fluid">
			<jdoc:include type="modules" name="breadcrumbs" style="none" />
		</div>
	</div>
</div>
<?php endif; ?>