<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if ($this->countModules('slider')) : ?>
<div class="clear-slider">
	<div class="clear-slider-wrap">		
		<jdoc:include type="modules" name="slider" style="none" />		
	</div>
</div>
<?php endif; ?>