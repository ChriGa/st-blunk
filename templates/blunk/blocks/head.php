<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');
$doc->addScript('templates/' . $this->template . '/js/template.js');
$doc->addScript('templates/' . $this->template . '/js/menu.js');
$doc->addScript('templates/' . $this->template . '/js/animsition.min.js');

// Add Stylesheets
$doc->addStyleSheet('templates/' . $this->template . '/css/template.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/overrides.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/responsive.css');
$doc->addStyleSheet('templates/' . $this->template . '/css/animsition.min.css');


// Load optional RTL Bootstrap CSS
JHtml::_('bootstrap.loadCss', false, $this->direction);

?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<?php if($itemid == "132") : //CG ios rendert die FAX nummer mit link, daher:  ?>
		<meta name="format-detection" content="telephone=no">
	<?php endif;?>	
	<jdoc:include type="head" />
	<?php // Use of Google Font ?>
	<?php /* if ($this->params->get('googleFont')) : ?>
		<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
	<?php endif; */ ?>
	
	<!--[if lt IE 9]>
		<script src="<?php echo $this->baseurl; ?>/media/jui/js/html5.js"></script>
	<![endif]-->