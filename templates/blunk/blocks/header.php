<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>

<header class="header">
	<div class="header-wrapper">
      <div class="container ">
		<div class="row-fluid">         
			<div class="header-inner clearfix">
				<div class="span7 pull-left visible-desktop">
					<jdoc:include type="modules" name="breadcrumbs" style="none" />
				</div>
				<div class="span5 logo pull-right visible-desktop">
					<?php if($logo) {  ?>
					<a class="brand animsition-link" href="<?php echo $this->baseurl; ?>">
						<?php print $logo; ?>
							<?php if ($this->params->get('sitedescription')) : ?>
								<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
							<? endif;?>
					</a> <?php } ?>
				</div>					
			</div>
			<nav class="navbar-wrapper">
				<div class="clear-phone-logo">
					<a class=" hidden-desktop " href="<?php echo $this->baseurl; ?>">
							<?php echo $logo; ?>
							<?php if ($this->params->get('sitedescription')) : ?>
								<?php echo '<div class="site-description">' . htmlspecialchars($this->params->get('sitedescription')) . '</div>'; ?>
							<?php endif; ?>
					</a>
				</div>
				<div class="clr"></div>
				<div id="kontaktinfo_mobile" class="span4 header-right pull-right">
					<jdoc:include type="modules" name="head-right" style="custom" />
				</div>
		        <div class="navbar">
			          <div class="navbar-inner">
						<div class="button_container" id="toggle">
						  <span class="top"></span>
						  <span class="middle"></span>
						  <span class="bottom"></span>
						</div>         		                         
						  <?php if ($this->countModules('menu')) : ?>
							<div class="nav-collapse collapse "  role="navigation">
								<jdoc:include type="modules" name="menu" style="custom" />
							</div>
							<?php endif; ?>
						  
			            <!--/.nav-collapse -->
			          </div><!-- /.navbar-inner -->
			        </div><!-- /.navbar -->
		    </nav>   
		</div>
      </div> <!-- /.container -->
    </div>
</header>