<?php
/**
 * @author   	ClearTemplates.com
 * @copyright   Copyright (C) 2015 ClearTemplates.com. All rights reserved.
 * @URL 		https://cleartemplates.com/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer id="footer" class="clear-footer" role="contentinfo">
	<div class="container clear-footer-wrap">		
		
		<div class="row-fluid">
			
					

			
			<?php if ($this->countModules('footer')) : ?>
			<div id="footer" class="span12">
				<jdoc:include type="modules" name="footer" style="none" />
			</div>
			<?php endif ?>
			
			<?php if ($this->countModules('footnav')) : ?>
			<div class="pull-right span4 footnav">
				<div class="module_footer position_footnav">
					<jdoc:include type="modules" name="footnav" style="none" />
				</div>			
			</div>
			<?php endif ?>			
			
		
		</div>
		
	</div>
</footer>
<div id="copyright" class="pull-left span6">
	<p>&copy; 2015 Steuerberaterin Starnberg / Pöcking Christina Blunk</p>
</div>
	
		